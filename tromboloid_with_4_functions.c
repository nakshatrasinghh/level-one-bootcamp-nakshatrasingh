//WAP to find the volume of a tromboloid using 4 functions.
#include<stdio.h>
float give_input(char a){
    float n;
    printf("enter the value of %c: ", a);
    scanf("%f", &n);
    return n;
}

float comp_volume(float h, float d, float b){
    float volume=(((h * d * b) + (d / b))/3);
    return volume;
}

void comp_output(float v){
    printf("\n the volume of tromboliod is: %f",v);
}

int main(){
    float h, d, b, v;
    h = give_input('h');
    d = give_input('d');
    b = give_input('b');
    v = comp_volume(h, d, b);
    comp_output(v);
    return 0;
}