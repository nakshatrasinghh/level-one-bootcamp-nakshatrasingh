#include <stdio.h>
#include <math.h>

struct point {
    float x;
    float y;
};

struct rectangle {
    struct point p[3];
    float area;
};

int inputSize() {
    	int n;
    	printf("Enter the number of rectangles: ");
    	scanf("%d", &n);
    	return n;
}

void inputPoints(int n, struct rectangle r[]) {
    	printf("Enter three points for each rectangle\n");
    	for (int i = 0; i < n; i++) {
        printf("Rectangle %d: ", i + 1);
        scanf("%f %f %f %f %f %f", &r[i].p[0].x, &r[i].p[0].y, &r[i].p[1].x,
			&r[i].p[1].y, &r[i].p[2].x, &r[i].p[2].y);
}
}

float computeDistance(struct point p1, struct point p2) {
    	return sqrt(pow((p2.x - p1.x), 2) + pow((p2.y - p1.y), 2));
}

void computeArea(int n, struct rectangle r[]) {
    	float length, breadth, diagonal;
    	for (int i = 0; i < n; i++) {
        		length = computeDistance(r[i].p[1], r[i].p[2]);
        		breadth = computeDistance(r[i].p[0], r[i].p[1]);
                r[i].area = length * breadth;
                }
}

void output(int n, struct rectangle r[]) {
    	printf("\n");
    	for (int i = 0; i < n; i++) {
        		printf("Area of rectangle with vertices (%.1f, %.1f), (%.1f, %.1f), (%.1f, %.1f) is %.1f\n", 
        		r[i].p[0].x, r[i].p[0].y, r[i].p[1].x, 
        		r[i].p[1].y, r[i].p[2].x,r[i].p[2].y, r[i].area);
                }
}

int main() {
    int n;
    n = inputSize();
    struct rectangle r[n];
    inputPoints(n, r);
    computeArea(n, r);
    output(n, r);
    return 0;
}