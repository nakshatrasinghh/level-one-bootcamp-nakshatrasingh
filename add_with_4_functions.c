//Write a program to add two user input numbers using 4 functions.
#include <stdio.h>
float give_input(){
    float n;
	printf("\n enter number: ");
	scanf("%f", &n);
	return n; 
}

float sum(float a, float b){
    float sum;
    sum = a + b;
	return sum; 
}

void dis_output(float n){
    printf("\n the sum is %f", n); 
}

int main(){
    float x, y, z;
    x=give_input();
    y=give_input();
    z=sum(x, y);
    dis_output(z);
    return 0; 
}
