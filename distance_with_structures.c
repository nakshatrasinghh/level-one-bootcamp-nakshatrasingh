//WAP to find the distance between two points using structures and 4 functions.
#include<stdio.h>
#include<math.h>
void input();
void calculate(float, float ,float ,float);
void display_out(float);
struct points {
	int x1,y1,x2,y2;
};
int main() {
input();
	return 0;
}
void input() {
	struct points p;
	printf("Enter values of x1, y1, x2, y2 respectively: ");
	scanf("%d %d %d %d", &p.x1, &p.y1, &p.x2, &p.y2);
	calculate(p.x1, p.y1, p.x2, p.y2);
}
void calculate(float a, float b, float c, float d) {
	float dist = sqrt((a-c) * (a-c) + (b-d) * (b-d));
	display_out(dist);
}
void display_out(float d) {
	printf("The total distance is: %f", d);
}