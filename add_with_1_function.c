//Write a program to add two user input numbers using one function.
#include <stdio.h>
int main()
{
  int number1, number2, sum;
  printf("Enter number: ");
  scanf("%d", &number1);
  printf("Enter number: ");
  scanf("%d", &number2);
 
  sum = number1 + number2;
  printf("Sum of %d and %d is %d.", number1, number2, sum);
  return 0;
}